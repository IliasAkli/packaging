#!/bin/bash

updatedb()  {
    echo "[INFO] - updating database"
    cd /opt/liquibase/sqlscripts/
    liquibase --driver="org.postgresql.Driver" \
        --changeLogFile=db.changelog.xml \
        --url="jdbc:postgresql://db/$DB_NAME" \
        --username=$DB_USERNAME --password=$DB_PASSWORD  \
        --classpath=$LIQUIBASE_HOME/postgresql-$POSTGRES_DRIVER_VERSION.jdbc41.jar \
        --logLevel=info \
        migrate
    return $?
}

i=1
max=5
sec=10

updatedb
while [ $? -ne 0 ] && [ $i -lt $max ]
do
    echo "[ERROR] - ["$i"] unable to update database $DB_NAME with user $DB_USERNAME waiting $sec seconds"
    i=$(( $i + 1 ))
    sleep $sec
    updatedb
done

if [ $i -eq $max ]
then
    echo "[ERROR] - unable to update database tried $i time"
    exit -1
fi

echo "[INFO] - Starting $@"
exec $@
