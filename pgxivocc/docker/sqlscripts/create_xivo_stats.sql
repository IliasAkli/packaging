CREATE USER asterisk WITH PASSWORD 'proformatique';
CREATE USER stats WITH PASSWORD 'stats';
CREATE DATABASE xivo_stats WITH OWNER asterisk;

CREATE LANGUAGE plpgsql;

\c xivo_stats
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
