#!/usr/bin/env python
# -*- coding: utf-8 -*-

from urllib2 import urlopen, Request
import sys, json

MAXPANELS="2000"

def make_get_request(url):
	request = Request(url)
	response = urlopen(request)
	return response

if len(sys.argv) < 2 :
	print("usage :  python export-kibana-dashboards.py http://localhost:9200 > file.json")
	exit()

elasticsearch_url = sys.argv[1]
query_url = elasticsearch_url + "/_search?q=dashboard:*&size=" + MAXPANELS
response = make_get_request(query_url)
data = json.loads(response.read().decode())
dashboards = data['hits']['hits']

print(json.dumps(dashboards, sort_keys=True, indent=4))
