#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from urllib.parse import urlencode, quote
from urllib.request import Request, urlopen
import sys, json

def make_post_request(url, data):
	header = {"Content-Type": "application/octet-stream", "Content-Length": str(len(data))}
	request = Request(url, data.encode(), header)
	response = urlopen(request)
	return response

if len(sys.argv) < 2 :
	print("usage :  python import-kibana-dashboards.py http://localhost:9200 < file.json")
	exit()

elasticsearch_url = sys.argv[1]
post_dashboard_url = elasticsearch_url + "/kibana-int/dashboard"
 
json_dashboards = sys.stdin.read()
dashboards = json.loads(json_dashboards)
 
for dashboard in dashboards:
	print("%s dashboard import..." % dashboard["_id"])
	data = json.dumps(dashboard['_source'])

	url = post_dashboard_url + "/" + quote(dashboard["_id"])
	print(make_post_request(url, data).read())

