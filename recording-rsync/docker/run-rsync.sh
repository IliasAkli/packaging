#!/bin/bash

ALLOW=${ALLOW:-192.168.0.0/16 172.16.0.0/12 10.0.0.0/8}
USERNAME=${USERNAME:-daemon}
PASSWORD=${PASSWORD:-password}

echo "$USERNAME:$PASSWORD" > /etc/rsyncd.secrets
chmod 400 /etc/rsyncd.secrets

cat <<EOF > /etc/rsyncd.conf
log file = /dev/stdout
max connections = 100
use chroot = yes

[recording]
    uid = daemon
    gid = daemon
    hosts deny = *
    hosts allow = ${ALLOW}
    path = /var/spool/recording-server
    auth users = $USERNAME
    secrets file = /etc/rsyncd.secrets
    read only = false
EOF

exec /usr/bin/rsync --no-detach --daemon --config /etc/rsyncd.conf "$@"
