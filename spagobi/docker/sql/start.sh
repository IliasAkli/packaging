#!/bin/bash

updatedb()  {
    cd /opt/liquibase/scripts/
    liquibase --driver="org.postgresql.Driver" \
        --changeLogFile=db.changelog.xml \
        --url="jdbc:postgresql://db/spagobi" \
        --username=spagobi --password=spagobi  \
        --classpath=$CATALINA_HOME/lib/postgresql-9.3-1102.jdbc41.jar \
        --logLevel=info \
        migrate
    return $?
}

i="0"
while [ $? -ne 0 ]; do
    i=$[$i+1]
    sleep 10
    updatedb()
done

echo "***Start tomcat"
#$CATALINA_HOME/bin/startup.sh
exit 0
