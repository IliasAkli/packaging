XiVOcc packaging project

* Build the fingerboard
* Docker yml file
* Javaliquibase docker image base for many components
* Kibana
* Postgres with database creation scripts
* Recording synchro (copy file)
* spagobi
* nginx


Update release in release.txt file

Build and dockerise using build.sh

Parameters :
* Component
* Docker tag (latest, latestdev, latestrc)


*Copyright Avencall 2016 : www.xivo.solutions*
